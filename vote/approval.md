# Voting

So, voting reform.
First off: why?
As a quick refresher on the current system:
everybody gets one vote, all votes are counted, and the candidate with the most votes wins.
This is often called first-past-the-post, plurality, or single-choice voting; I'll call it FPTP for short.

What's wrong with FPTP?

### Theoretical Considerations

One problem that many people run into is third parties:
FPTP _strongly_ encourages a two party system,
mostly by splitting votes between any parties that end up appealing to the same voters.
For example, a third party that is close to one of the two major parties will generally pull votes away from that party,
splitting the vote and allowing the other main party to win elections that it otherwise wouldn't.

![Diagram showing A support pulling votes from B, allowing C to win](diagram_01_split.png "Voters defecting from B to A cause C to win")

Consequently, voters tend to avoid third parties entirely.

Ranked choice voting (often called instant-runoff or Hare; I'll call it RCV) is a commonly proposed solution.
It's much more complicated than FPTP, but it directly addresses throwing votes away on third parties:
everybody ranks all the candidates they're voting for, first-choices are tabulated, the majority winner is chosen.
In the likely event that there isn't a majority winner, the lowest-scoring candidate is eliminated, and each of their votes is given to the next preferred candidate.
These "instant runoff" elimination rounds continue until a majority winner exists.

It's easy to see how this would fix the previously mentioned problem:
people ranking the third party first would likely rank the closer large party second,
so when the third party gets eliminated in the early rounds their votes transfer to closest competive candidate.
Vote-splitting solved.

![Diagram showing A votes falling through to B, allowing B to win](diagram_02_xfer.png "Voters defecting from B to A can mark B second, allowing B to win")

At least, it's solved for non-competitive elections:
if you _know_ certain candidates will be eliminated early,
then you can vote for them with impunity;
ordering between competitive options can get difficult.

If the hypothetical third party and the closer major party each have about the same first-round vote totals,
then things get interesting.
One party isn't guaranteed to lose, and the ordering on a ticket matters again.
The "third party first, fallback to major party" strategy risks eliminating the major party first,
potentially sinking the third party if a significant fraction of major party voters don't rank it next.

![Diagram showing B getting eliminated before A can transfer any votes](diagram_03_close.png "Voters defecting from B to A cause B to get eliminated first; if B voters don't also vote for A, then C wins")

In this scenario, a strategic voter would decide to put the major party first,
even if they prefer the third party,
bringing us back to the same dynamics that so many people hate from FPTP[^worse].

![Diagram showing B votes failing to fall through to A](diagram_04_strategy.png "Since B still has more overall support than A, strategic A voters are forced to rank B before A to prevent C from winning")

The underlying issue is that each ballot is a zero-sum system: giving a vote or rank to one candidate means that another candidate can't have it.
Voters effectively run a mini-election to fill out their ballots, choosing the winner that they then think can win the election itself.

The _real_ solution to this problem is to decouple candidates from each other by allowing independent scoring for each candidate.
This type of approach is called cardinal voting[^cardinal] and any cardinal voting system solves the vote-splitting problem.

Among all the different cardinal voting methods, my preferred system is approval voting.
Before getting to why, let's take a quick look at what RCV voting looks like in practice.

### Practical Considerations

My city currently uses RCV for some elections, and one of the races on the 2020 ballot looks like this:

![Example of 3x5 RCV ranking matrix for 2 candidates](diagram_11_sparse.png "RCV ballot")

In a three-person race (including the write-in spot), there are options to rank candidates as 4th and 5th most preferred.
It's unclear whether doing so would invalidate the ballot, so I'm not going to risk putting my least preferred candidate in 5th place,
but the fact that the ballot itself allows that is _weird_.

Anybody who knows the details of the 2000 election knows how important clear, simple ballots are,
and RCV ballots are inherently complicated.
In a simple three-way race (without extra rankings, as seems to happen),
there are over 500 ways to fill out the ballot,
of which only 6 (total ranking required; 12 if implicit final ranking is allowed), 15 (partial ranking allowed), or 21 (fallthrough from missing first place allowed) are valid.

![Example of two out of three candidates ranked on an RCV ballot, with A ranked first and B ranked second](diagram_12_3x3rcv.png "One probably-valid way of filling out a 3-way RCV ballot")

The largest RCV race on my ballot has seven candidates and five rankings (which is weird by itself),
so there's about 34 million ways to fill in bubbles and only 6139 valid ways to vote.

All of this complexity means that RCV ballots are about 10x more likely to be invalid than FPTP ballots[^spoil],
disenfranchising a significant amount of voters.

Unfortunately, similar problems around ballot complexity crop up with many of the clever voting systems that people have come up with,
and the more clever the system the less likely voters will even understand it in the first place.
For example, many people would likely confuse scored and ranking systems, scoring candidates in preference order instead of giving favored candidates the highest scores.

![Example of an ambiguous ballot with votes for A and B](diagram_13_ambiguous.png "Either a valid scored ballot or an invalid ranked ballot")

For all its faults, at least FPTP is simple.
But what if we could simplify it further?

### Approval voting

Here's a recap of FPTP:

- Voters vote for one candidate
- Any ballot with more than one vote is invalid
- Valid votes are counted
- The winner is the candidate with the most votes

![Example of a valid FPTP ballot, with a vote for B](diagram_21_fptp.png "A valid FPTP ballot")

Approval voting is just like FPTP, but without the single candidate restriction:

- Voters vote for any number of candidates
- Votes are counted
- The winner is the candidate with the most votes

![Example of a valid approval ballot, with votes for A and B](diagram_22_approval.png "A valid approval ballot")

By simplifying a bad system, we get something that's not only more practical but also more theoretically sound:

- Difficult to invalidate a ballot: even if the voter assumes it's FPTP, the single vote will still be counted.
- No spoiler effect: voters can vote for as many long-shots as they want, and still vote for a competitive candidate; all vote totals get counted independently.
- Simplifies voting decisions: instead of requiring ranking between candidates, voters only need to consider if they want the candidate to win or not.

No system is perfect -- Gibbard's theorem[^thm] proves that every voting system has _some_ undesirable properties -- but approval is clearly the best compromise.

The biggest "weakness" of approval voting -- the inability to express prefences beyond "approve" and "disapprove" -- is, given the imperfect nature of elections, actually a feature.
As discussed above, any ballot more complicated than FPTP will disenfranchise some additional fraction of voters, potentially negating any benefits.
Ranked ballots give voters the opportunity to shoot themselves in the foot, where ranking a "safe" candidate behind a riskier favorite could result in both candidates being prematurely eliminated.

Approval voting solves problems that RCV can't, and does it while being more simple to understand, implement, and run.


[^worse]: Technically, the dynamics are actually much worse. Strategic A voters voting for B would be pretty mild; strategic C voters voting for A to eliminate B would be diabolical, but entirely possible under RCV.

[^cardinal]: From the economic concept of cardinal utility https://en.wikipedia.org/wiki/Cardinal_utility

[^spoil]: Rates of ~1% vs ~10% according to https://www.themainewire.com/2018/05/ranked-choice-voting-expect-spike-spoiled-ballots/

[^thm]: Gibbard extended Arrow's impossibility theorem, which originally only applied to non-cardinal voting systems https://en.wikipedia.org/wiki/Gibbard%27s_theorem

